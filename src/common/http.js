import * as axios from "axios";

export const HTTP = axios.create({
  baseURL: 'http://192.168.0.55:8000/api',
  headers: {
    contentType: 'application/json'
  }
})