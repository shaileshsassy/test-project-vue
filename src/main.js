import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import * as filter from './common/filter'

import VueMaterial from 'vue-material'
import 'vue-material/dist/vue-material.min.css'
import 'vue-material/dist/theme/default.css'

Vue.use(VueMaterial)

Vue.config.productionTip = false

router.beforeEach((to, from, next) => {
  if (to.meta.requiresAuth) {
    if (window.localStorage.getItem('Authorization') == null) {
      next({name:'login'})
      return
    }
  }

  next()
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
